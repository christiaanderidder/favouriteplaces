﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FavouritePlaces.Models
{
    public class Destination : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public float Lat { get; set; }
        public float Lon { get; set; }

        public List<Image> Images { get; set; }
        public List<Link> Links { get; set; }
    }
}
