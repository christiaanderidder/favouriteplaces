﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FavouritePlaces.Models
{
    public class Image : BaseEntity
    {
        public string Description { get; set; }
        public string Url { get; set; }

        public Destination Destination { get; set; }
    }
}
