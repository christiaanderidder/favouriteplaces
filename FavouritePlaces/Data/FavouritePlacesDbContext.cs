﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FavouritePlaces.Models;
using Microsoft.EntityFrameworkCore;

namespace FavouritePlaces.Data
{
    public class FavouritePlacesDbContext : DbContext
    {
        public FavouritePlacesDbContext(DbContextOptions<FavouritePlacesDbContext> options) : base(options)
        {
            
        }

        public DbSet<Destination> Destinations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Destination>().HasKey(d => d.Id);
            modelBuilder.Entity<Image>().HasOne(p => p.Destination).WithMany(b => b.Images);
            modelBuilder.Entity<Image>().HasKey(i => i.Id);
            modelBuilder.Entity<Link>().HasOne(p => p.Destination).WithMany(b => b.Links);
            modelBuilder.Entity<Link>().HasKey(i => i.Id);
        }

        public override int SaveChanges()
        {
            SetTimeStamps();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            SetTimeStamps();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SetTimeStamps();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            SetTimeStamps();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void SetTimeStamps()
        {
            var entities = ChangeTracker.Entries();
            foreach (var entry in entities)
            {
                var baseEntity = entry.Entity as BaseEntity;
                if (baseEntity == null) continue;

                if (entry.State == EntityState.Added)
                {
                    baseEntity.CreatedAt = DateTime.UtcNow;
                }

                baseEntity.UpdatedAt = DateTime.UtcNow;
            }
        }
    }
}
