﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FavouritePlaces.Data;
using FavouritePlaces.Models;
using FavouritePlaces.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace FavouritePlaces.Controllers
{
    [Route("api/[controller]")]
    public class DestinationsController : Controller
    {
        private const int PageSize = 10;

        private readonly FavouritePlacesDbContext _ctx;

        public DestinationsController(FavouritePlacesDbContext ctx)
        {
            _ctx = ctx;
        }

        /// <summary>
        /// List all destinations
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(CollectionApiResponse<Destination>), 200)]
        public IActionResult Index([FromQuery]int page = 1)
        {
            var pageIndex = Math.Max(0, page - 1);
            
            var items = _ctx.Destinations.Skip(pageIndex * PageSize).Take(PageSize).ToList();
            var totalCount = _ctx.Destinations.Count();

            return Ok(new CollectionApiResponse<Destination>(items)
            {
                Page = page,
                PageCount = PageSize,
                TotalCount = totalCount
            });
        }

        /// <summary>
        /// Get a single destination by ID
        /// </summary>
        /// <param name="id">The destination ID</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(SingleApiResponse<Destination>), 200)]
        public IActionResult Get(int id)
        {
            var dst = _ctx.Destinations.Find(id);
            return Ok(new SingleApiResponse<Destination>(dst));
        }

        /// <summary>
        /// Add a new destination
        /// </summary>
        /// <param name="destination">The new destination</param>
        [HttpPost]
        public IActionResult Post([FromBody]DestinationViewModel destination)
        {
            var dst = new Destination()
            {
                Name = destination.Name,
                Description = destination.Description,
                Lat = destination.Lat,
                Lon = destination.Lon
            };
            _ctx.Destinations.Add(dst);
            _ctx.SaveChanges();

            return CreatedAtAction(nameof(Get), new { dst.Id});
        }

        /// <summary>
        /// Update a destination
        /// </summary>
        /// <param name="id">The destination ID</param>
        /// <param name="destination">The updated destination</param>
        [HttpPut("{id}")]
        public IActionResult Put(int id, Destination destination)
        {
            _ctx.Destinations.Update(destination);
            _ctx.SaveChanges();

            return Ok();
        }

        /// <summary>
        /// Delete a destination
        /// </summary>
        /// <param name="id">The destination ID</param>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var dest = _ctx.Destinations.Find(id);
            _ctx.Destinations.Remove(dest);
            _ctx.SaveChanges();

            return NoContent();
        }
    }
}
