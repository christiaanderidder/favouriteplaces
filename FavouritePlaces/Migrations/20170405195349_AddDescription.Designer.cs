﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using FavouritePlaces.Data;

namespace FavouritePlaces.Migrations
{
    [DbContext(typeof(FavouritePlacesDbContext))]
    [Migration("20170405195349_AddDescription")]
    partial class AddDescription
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("FavouritePlaces.Models.Destination", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Description");

                    b.Property<float>("Lat");

                    b.Property<float>("Lon");

                    b.Property<string>("Name");

                    b.Property<DateTime>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("Destinations");
                });

            modelBuilder.Entity("FavouritePlaces.Models.Image", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Description");

                    b.Property<int?>("DestinationId");

                    b.Property<DateTime>("UpdatedAt");

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.HasIndex("DestinationId");

                    b.ToTable("Image");
                });

            modelBuilder.Entity("FavouritePlaces.Models.Image", b =>
                {
                    b.HasOne("FavouritePlaces.Models.Destination", "Destination")
                        .WithMany("Images")
                        .HasForeignKey("DestinationId");
                });
        }
    }
}
