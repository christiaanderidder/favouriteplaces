﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FavouritePlaces.ViewModels
{
    public class DestinationViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public float Lat { get; set; }
        public float Lon { get; set; }
    }
}
