﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;

namespace FavouritePlaces.ViewModels
{
    public class CollectionApiResponse<T> : ApiResponse
    {
        public CollectionApiResponse(IEnumerable<T> data, string message = null) : base(message)
        {
            Data = data;
        }

        /// <summary>
        /// The total number of items
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// The number of items on this page
        /// </summary>
        public int PageCount { get; set; }

        /// <summary>
        /// The current page
        /// </summary>
        public int Page { get; set; }
        
        public IEnumerable<T> Data { get; set; }
    }
}
