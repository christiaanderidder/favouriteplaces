﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FavouritePlaces.ViewModels
{
    public class SingleApiResponse<T> : ApiResponse
    {
        public SingleApiResponse(T data, string message = null) : base(message)
        {
            Data = data;
        }

        public T Data { get; set; }
    }
}
